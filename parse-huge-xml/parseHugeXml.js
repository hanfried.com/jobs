const { open, close ,stat } = require('node:fs/promises')
const { XMLParser } = require("fast-xml-parser");

const defaultBufferSize = 1024;
class parseHugeXml {
    constructor(fileName,nodeName) {
        this.xmlFileName = fileName
        this.parseNodeName = nodeName
        this.preNodeEndSeek = 0
        this.preNodeStartSeek = 0
        this.currentNodeEndSeek = 0
        this.currentNodeStartSeek = 0
        this.currentSeek = 0
    }
    async readFileWithSeek(bufferSize,currentSeek){
        if(!this.xmlFile){
            this.xmlFile = await open(this.xmlFileName,'r')
            this.xmlFileInfo = await stat(this.xmlFileName)
        }

        const buffer = new Buffer.alloc(bufferSize);
        // (buffer, buffer_offset, read_length, read_position)
        return await this.xmlFile.read(buffer,0,bufferSize,currentSeek)
    }

    async searchNextNodeSeek(){
        let nodeStrs = this.nodeStrs()
        let searchResult = {'start':0,'end':0}
        while(true){
            let readedBuffer = await this.readFileWithSeek(defaultBufferSize,this.currentSeek)
            if(readedBuffer.bytesRead === 0){
                return {}
            }
            let currentContent = readedBuffer.buffer;
            if (searchResult.start === 0){
                let start = currentContent.indexOf(nodeStrs.start)
                if(start >= 0){
                    searchResult.start = this.currentSeek+start
                }
            }
            if (searchResult.start !== 0 && searchResult.end === 0){
                let end = currentContent.indexOf(nodeStrs.end)
                if (end >= 0 && this.currentSeek+end > searchResult.start){
                    searchResult.end = this.currentSeek+end
                    //await this.debug(searchResult.end,20)
                }
            }
            if (searchResult.start !==0 && searchResult.end !== 0){
                this.preNodeStartSeek = this.currentNodeStartSeek
                this.preNodeEndSeek = this.currentNodeEndSeek

                this.currentNodeStartSeek = searchResult.start
                this.currentNodeEndSeek = searchResult.end

                this.currentSeek = searchResult.end + nodeStrs.end.length
                break
            }

            // sometimes read buffer split node like "xxxxx<nod" "e>xxxx"
            // so need double read
            if(readedBuffer.bytesRead - (nodeStrs.end.length * 2) <= 0){
                return {}
            }
            this.currentSeek += readedBuffer.bytesRead - (nodeStrs.end.length * 2)
        }
        return searchResult
    }
    nodeStrs(){
        return {
            start:'<'+this.parseNodeName+'>',
            end:'</'+this.parseNodeName+'>'
        }
    }
    async searchGetNextNodeContent(){
        const nextSeekRange = await this.searchNextNodeSeek()
        if (!nextSeekRange.start || !nextSeekRange.end){
            return ""
        }
        console.log('[find node]',nextSeekRange)
        const readedBuffer = await this.readFileWithSeek(nextSeekRange.end - nextSeekRange.start+this.nodeStrs().end.length,nextSeekRange.start)
        if(readedBuffer.bytesRead === 0){
            return ""
        }
        return readedBuffer.buffer.toString()
    }
    async parseNextNode(){
        let nextNodeContent = await this.searchGetNextNodeContent()
        if (nextNodeContent === ""){
            return undefined
        }
        return this.parseNode(nextNodeContent)
    }
    parseNode(data){
        const parser = new XMLParser();
        return parser.parse(data);
    }
    async debug(seek,length){
        const readedBuffer = await this.readFileWithSeek(length,seek)
        console.log('[debug]',readedBuffer.buffer.toString())
    }
    async parseAllNodes(){
        if (!this.xmlFileInfo){
            this.xmlFileInfo = await stat(this.xmlFileName)
        }
        let allNodes = [];
        while(this.currentSeek < this.xmlFileInfo.size){

            let currentNode = await this.parseNextNode()
            if (!currentNode){
                break
            }else{
                allNodes.push(currentNode)
            }
        }
        return allNodes
    }

}
exports.parseHugeXml = parseHugeXml

